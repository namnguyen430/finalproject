//
//  ColorViewController.swift
//  finalproject_inprogress
//
//  Created by Soo Jung Aguilar on 11/30/21.
// following chunk of tutorial : Swift 5.1 How to make Drawing App (Xcode 11.1) from Let Create An App

import UIKit
import Photos
import CoreData

// IMPORTANT NOTES!!
// - To color an image, that line art must be TRANSPARENT!!
// - When you save lineArt, the background SHOULD be transparent. In theory. IDK how apple saves images.

// BUG NOTES
// Melina : here are all the bugs I've found and I will mark them out as I figure them out
// - does not save to camera roll or core data first time it asks for permission
// - color picker does not add new color if player swipes down. Technically that dismisses so it's working as it's suppose to but it's still annoying

class ColorViewController: UIViewController, UIColorPickerViewControllerDelegate {
    
    // connect view to the CanvasView file
    @IBOutlet weak var canvasView: CanvasView!
    // use #colorLiteral() to hand pick colors. This is a neat function :D
    
    var colorList: [UIColor] = [#colorLiteral(red: 1, green: 1, blue: 1, alpha: 1), #colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1), #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)]
    
    var newColor : UIButton!
    
    var isColor : Bool!
    
//    public enum Page{
//        case forLineArt
//        case forColoring
//    }

//    public var currentPage: Page = .forLineArt{
//    didSet{
//        Views(pageType: currentPage)
//        }
//    }
    @IBOutlet weak var lineBackground: UIView!
    
    @IBOutlet weak var colorView: UICollectionView!
    
    @IBOutlet weak var undoButton: UIButton!
    @IBOutlet weak var redoButton: UIButton!
    @IBOutlet weak var pencilTool: UIButton!
    
    @IBOutlet weak var openMenuButton: UIButton!
    
    @IBOutlet weak var widthSliderObj: UISlider!
    @IBOutlet weak var hamburgerStack: UIStackView!
    
    @IBOutlet weak var tipSegCntrl: UISegmentedControl!
    
    // hides and shows ui assets depending on page
//    private func Views(pageType:Page){
//        lineBackground?.isHidden = (pageType == .forColoring)
//    }
    
    var hueF: CGFloat!
    var satF: CGFloat!
    var brightF: CGFloat!
    var addingColor: UIColor!
    var emptyColor:UIColor!
    
    //var coloringImage : UIImage!
    var bgImage: UIImage!
    var delegate: UIViewController!

    let myLayer = CALayer()
    
    // start func
    override func viewDidLoad() {
        super.viewDidLoad()
        if isColor == true{
            let coloringImage = bgImage
            
            let imageView = UIImageView(image: coloringImage)
            imageView.contentMode = UIView.ContentMode.scaleAspectFit
            imageView.frame = CGRect(x: canvasView.frame.origin.x - 50, y: canvasView.frame.origin.y - 50, width: canvasView.frame.width, height: canvasView.frame.height)//canvasView.convert(imageView.frame, to: self.view)
            imageView.clipsToBounds = true
            lineBackground.isHidden = true
            canvasView.addSubview(imageView)
        }else if isColor == false{
            print("LINEART LOAD")
            canvasView.backgroundColor = UIColor.clear
            lineBackground.isHidden = false
        }
        
        
        colorView.delegate = self
        colorView.dataSource = self
        colorView.backgroundColor =  UIColor(red: 0.90, green: 0.99, blue: 0.95, alpha: 1)
        widthSliderObj.isHidden = true
        hamburgerStack.isHidden = true
        tipSegCntrl.isHidden = true
        emptyColor = UIColor.clear
        
        canvasView.capVar = .round
        
        // lets add some colorsss
        hueF = 0
        satF = 0
        brightF = 0
        
        // nested for loop that adds 4 colors for every change in hue. Its... its a lot of colors....
        // healthy amount of colors, but maybe we should do less
        for hues in 1...10{
            hueF = 0 + (0.1 * CGFloat(hues))
            satF = 0.20
            brightF = 0.20
            for colors in 1...5{
                addingColor = UIColor(hue: hueF, saturation: satF, brightness: brightF, alpha: 1)
                colorList.insert(addingColor, at: 0)
                //print("added color")
                if (brightF + 0.20) <= 1 {
                    satF += 0.20
                    brightF += 0.20
                }
            }
        }
    }
    // opens the color picker (new view controller). I think of it as like,, a half view controller. Like, it exists in the same space as this one but it needs to pass an object back in forth.
    @IBAction func addColor(_ sender: Any) {
        let colorPicker = UIColorPickerViewController()
        colorPicker.delegate = self
        present(colorPicker,animated:true)
        //colorPicker.selectedColor = emptyColor
        self.colorView.reloadData()
    }
    
    // adds the new color from the color picker
    func colorPickerViewControllerDidFinish(_ viewController: UIColorPickerViewController) {
        let color = viewController.selectedColor
        colorList.insert(color, at: 0)
        colorView.reloadData()
    }
    
    // buttons ! --------------------------------------------------------------------------
    
    // opens menu. It's just like in Unity.
    @IBAction func onClick_openMenu(_ sender: Any) {
        if hamburgerStack.isHidden == true
        {
            hamburgerStack.isHidden = false // turn on menu.
        }else{
            hamburgerStack.isHidden = true // else turn it off.
        }
    }
    
    // undo button
    @IBAction func onClick_undo(_ sender: Any) {
        canvasView.undoFunc()
    }
    
    // opens segment controller
    @IBAction func changeTip(_ sender: Any) {
        if tipSegCntrl.isHidden == true{
            tipSegCntrl.isHidden = false
        }else{
            tipSegCntrl.isHidden = true
        }
    }
    
    
    @IBAction func tipSegC(_ sender: Any) {
        switch tipSegCntrl.selectedSegmentIndex{
        case 0:
            canvasView.capVar = .round
        case 1:
            canvasView.capVar = .square
        default:
            canvasView.capVar = .round
        }
    }
    
    
    // clears canvas, but asks you if you are sure first
    @IBAction func newCanvas(_ sender: Any) {
        let controller = UIAlertController(
            title: "Clear cavas",
            message: "Are you sure you want to delete your drawing?",
            preferredStyle: .alert)
        controller.addAction((UIAlertAction(
                                title: "Yes",
                                style: .default,
                                handler: {action in
                                    self.canvasView.newFunc()
                                })))
        controller.addAction(UIAlertAction(
                                title: "No",
                                style: .cancel,
                                handler: nil
                                ))
        present(controller, animated: true, completion: nil)
        
    }
    
    
    // hides or opens width slider.
    @IBAction func onClick_pencilTool(_ sender: Any) {
        if widthSliderObj.isHidden{
            widthSliderObj.isHidden = false // turn on slider
        }
        else{
            widthSliderObj.isHidden = true
        }
    }
    
    
    
    
    // slider value is passed to the CanvasView Class.
    @IBAction func widthChange(_ sender: UISlider) {
        canvasView.strokeWidth = CGFloat(sender.value)
    }
    
    
    // save image button :
    @IBAction func saveImage(_ sender: Any) {
        // check current status of permission to get photos.
        let photos = PHPhotoLibrary.authorizationStatus()
        // if the status is unknown, request access
        if photos == .notDetermined{
            PHPhotoLibrary.requestAuthorization({status in
                if status == .authorized{
                    
                } else{}
            })
        }
        // if access is authorized, save function
        if PHPhotoLibrary.authorizationStatus() == .authorized{
            //canvasView.isOpaque = false
            //canvasView.backgroundColor = UIColor.clear
            let imageData = canvasView.saveImage()
            
            UIImageWriteToSavedPhotosAlbum(imageData, self, #selector(imageSaved(_:didFinishSavingWithError:contextType:)), nil)
            
            //saveCoreData(image: imageData)
            
            let controller = UIAlertController(
                title: "Saved",
                message: "Your picture has been saved",
                preferredStyle: .alert)
            controller.addAction((UIAlertAction(
                                    title: "Ok",
                                    style: .default,
                                    handler: nil
            )))
            present(controller, animated: true, completion: nil)
        }
    }
    
    @IBAction func onClick_Back(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func onClick_Post(_ sender: Any) {
        let imageData = canvasView.saveImage()
        saveCoreData(image: imageData)
    }
    
    // buttons end ------------------------------------------------------------------------
    // CORE DATA SAVING RIGHT HERE!!!!!!
    func saveCoreData(image: UIImage)
    {
        let imageData = image.pngData()
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
        
        if isColor == true{
            let savingImage = NSEntityDescription.insertNewObject(forEntityName: "Color_Image", into: context)
            savingImage.setValue(imageData, forKey: "imageData")
        }else if isColor == false{
            let savingImage = NSEntityDescription.insertNewObject(forEntityName: "LineArt_Image", into: context)
            savingImage.setValue(imageData, forKey: "imageData")
        }
        
        
        
        do {
            try context.save()
        } catch {
            // If an error occurs
            let nserror = error as NSError
            NSLog("Unresolved error \(nserror), \(nserror.userInfo)")
            abort()
        }
    }
    // saves image
    @objc func imageSaved(_ image:UIImage,didFinishSavingWithError error: Error?, contextType: UnsafeRawPointer){
        if error != nil{
            print("SAVING")
        }
        else{
        }
    }
}



// creates a collection view of our list of colors
// needs a make over. So ugly looking rn.
extension ColorViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return colorList.count
    }
    
    // creates cells based on the number of colors in the color list. Will need to reload data once we add a new color.
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath)
        if let view = cell.viewWithTag(111) as? UIView {
            view.backgroundColor = colorList[indexPath.row]
            view.layer.cornerRadius = 1
            
            
        }
        return cell
    }
    
    // turns the color of the pen to th ecolor that was selected in the Colors View
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        canvasView.strokerColor = colorList[indexPath.row]
    }
    
}


// turns the UI view with the art into a UI Image so we can save
extension UIView {
    func saveImage() -> UIImage{
        UIGraphicsBeginImageContextWithOptions(self.bounds.size, false, UIScreen.main.scale)
        drawHierarchy(in: self.bounds, afterScreenUpdates: true)

        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()

        if image != nil{
            return image!
        }
        return UIImage()
    }
    
}
