//
//  ExploreViewController.swift
//  finalproject_inprogress
//
//  Created by Soo Jung Aguilar on 12/3/21.
//

import UIKit
import CoreData

class ExploreViewController: UIViewController {  //UICollectionViewDelegate, UICollectionViewDataSource
    @IBOutlet weak var imageCollectionView: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //clearCoreData()
        
//        let fetchedResults = retrieveImages()
//        convertDataToImages(imageDataArray: fetchedResults)
//
//        for person in fetchedResults {
//            if let personName = person.value(forKey:"name") {
//                if let personAge = person.value(forKey:"age") {
//                    print("Retrieved: \(personName), age \(personAge)")
//                }
//            }
//        }

        // Do any additional setup after loading the view.
    }
    
//    func retrieveImages() -> [Data]{
//        let appDelegate = UIApplication.shared.delegate as! AppDelegate
//        let context = appDelegate.persistentContainer.viewContext
//
//        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "Color_Image")
//        do {
//          result = try managedContext.fetch(fetchRequest)
//          for data in result as! [NSManagedObject] {
//             imageDataArray.append(data.value(forKey: "storedImage") as! Data)
//          }
//        } catch let error as NSError {
//          print("Could not fetch. \(error), \(error.userInfo)")
//        }
//
//        //fetch image using decoding
//        imageDataArray.forEach { (imageData) in
//          var dataArray = [Data]()
//          do {
//            dataArray = try NSKeyedUnarchiver.unarchivedObject(ofClass: NSArray.self, from: imageData) as! [Data]
//            myImagesdataArray.append(dataArray)
//          } catch {
//            print("could not unarchive array: \(error)")
//          }
//        }
        
//        let request = NSFetchRequest<NSFetchRequestResult>(entityName:"Color_Image")
//        var fetchedResults:[NSManagedObject]? = nil
//        do {
//            try fetchedResults = context.fetch(request) as? [NSManagedObject]
//        } catch {
//            // If an error occurs
//            let nserror = error as NSError
//            NSLog("Unresolved error \(nserror), \(nserror.userInfo)")
//            abort()
//        }
//
//        return(fetchedResults)!
    
    func convertDataToImages(imageDataArray: [Data]) -> [UIImage] {
      var myImagesArray = [UIImage]()
      imageDataArray.forEach { (imageData) in
          myImagesArray.append(UIImage(data: imageData)!)
      }
      return myImagesArray
    }
    
//    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
//        <#code#>
//    }
//
//    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
//        <#code#>
//    }
    


//Second website

    func clearCoreData() {
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
        
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Color_Image")
        var fetchedResults:[NSManagedObject]
        
        do {
            try fetchedResults = context.fetch(request) as! [NSManagedObject]
            
            if fetchedResults.count > 0 {
                
                for result:AnyObject in fetchedResults {
                    context.delete(result as! NSManagedObject)
                    print("deleted")
                    //print("\(result.value(forKey: "name")!) has been Deleted")
                }
            }
            try context.save()
            
        } catch {
            // If an error occurs
            let nserror = error as NSError
            NSLog("Unresolved error \(nserror), \(nserror.userInfo)")
            abort()
        }
    }
}
