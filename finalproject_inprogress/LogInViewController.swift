//
//  LogInViewController.swift
//  finalproject_inprogress
//
//  Created by Soo Jung Aguilar on 11/28/21.
//

import UIKit
import Firebase
import FirebaseDatabase

class LogInViewController: UIViewController {
    
    //Text Fields
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var confirmTextField: UITextField!
    
    //Buttons
    @IBOutlet weak var logInButton: UIButton!
    @IBOutlet weak var signUpButton: UIButton!
    @IBOutlet weak var backButton: UIButton!
    
    //Text
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var confirmPasswordLabel: UILabel!
    @IBOutlet weak var accountLabel: UILabel!
    
    var buttonState = "Log in"
    let segueIdentifier = "LogInSegueIdentifier"
    
    //For Launch Animation
    @IBOutlet weak var logoTextImage: UIImageView!
    @IBOutlet weak var logoSymbolView: UIImageView!
    @IBOutlet weak var bgView: UIView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        //Hide sign up elements on log in screen
        showLogIn()
        
        //Log in user automatically
        Auth.auth().addStateDidChangeListener() {
          auth, user in

          if user != nil {
              self.performSegue(withIdentifier: self.segueIdentifier, sender: nil)
            self.emailTextField.text = nil
            self.passwordTextField.text = nil
          }
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.logoTextImage.frame.origin.x = -(self.logoTextImage.frame.size.width)
        self.logoSymbolView.alpha = 0
        
        UIView.animate( withDuration: 0.5, delay: 0.5, options: .curveEaseOut,
            animations: {
                self.logoTextImage.frame.origin.x += 84 + self.logoTextImage.frame.size.width
            },
            completion: { finished in
                //Button fades in
                UIView.animate( withDuration: 0.5, delay: 0.0, options: .curveEaseIn,
                    animations: {
                    self.logoSymbolView.alpha = 1.0
                },
                completion: { finished in
                    UIView.animate( withDuration: 0.5, delay: 0.3, options: .curveEaseIn,
                        animations: {
                        self.logoTextImage.frame.origin.x = -(self.logoTextImage.frame.size.width)
                        self.logoSymbolView.frame.origin.x = 420
                        self.bgView.frame.origin.y = -(self.bgView.frame.size.height)
                    },
                    completion: nil)
                })
                print("Animation finished")
            })
    }
    
    func showLogIn() {
        //Log in screen
        backButton.isHidden = true
        nameTextField.isHidden = true
        confirmTextField.isHidden = true
        nameLabel.isHidden = true
        confirmPasswordLabel.isHidden = true
        
        //Show log in elements that are hidden on sign up screen
        accountLabel.isHidden = false
        signUpButton.isHidden = false
        buttonState = "Log in"
        UIView.performWithoutAnimation {
            self.logInButton.setTitle(buttonState, for: .normal)
            self.logInButton.layoutIfNeeded()
        }
    }
    
    @IBAction func pressedBackButton(_ sender: Any) {
        showLogIn()
    }
    
    @IBAction func pressedLogIn(_ sender: Any) {
        
    }
    
    @IBAction func pressedSignUp(_ sender: Any) {
        //This is the small text button to sign up for an account on the log in screen
        //Sign up screen
        backButton.isHidden = false
        nameTextField.isHidden = false
        confirmTextField.isHidden = false
        nameLabel.isHidden = false
        confirmPasswordLabel.isHidden = false
        
        //Hide log in elements
        accountLabel.isHidden = true
        signUpButton.isHidden = true
        buttonState = "Sign up"
        UIView.performWithoutAnimation {
            self.logInButton.setTitle(buttonState, for: .normal)
            self.logInButton.layoutIfNeeded()
        }
    }
    
    // code to enable tapping on the background to remove software keyboard
        func textFieldShouldReturn(textField:UITextField) -> Bool {
            textField.resignFirstResponder()
            return true
        }

        override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
            self.view.endEditing(true)
        }
    
    //Firebase
    func didUserLoggedInSuccessfully(success:@escaping (Bool, Error?) -> Void) {
        Auth.auth().signIn(withEmail: self.emailTextField.text!, password: self.passwordTextField.text!) { (user, error) in
            if error == nil {
                success(true,nil)
            }
            else{
                success(false,error)
            }
        }
        let storageRef = Storage.storage().reference()
        
        var dbRef: DatabaseReference!
        dbRef = Database.database().reference()
        
        let usersReference = dbRef.child("users")
        let uid = user?.uid
        let newUserReference = usersReference.child(uid!)
        newUserReference.setValue(["name": self.nameTextField.text,"email": self.emailTextField.text])
        
    }
    
    override func shouldPerformSegue(withIdentifier identifier: String, sender: Any?) -> Bool {
        if identifier == segueIdentifier {
            if buttonState == "Log in" {
                guard let email = emailTextField.text,
                      let password = passwordTextField.text,
                      email.count > 0,
                      password.count > 0
                else {
                    //An alert with the error message will pop up
                    let controller = UIAlertController(
                        title: "Log in failed",
                        message: "Email and password cannot be blank",
                        preferredStyle: .alert)
                    controller.addAction(UIAlertAction(
                                            title: "OK",
                                            style: .default,
                                            handler: nil))
                    present(controller, animated: true, completion: nil)
                    return false
                }
                self.didUserLoggedInSuccessfully { (result,error) in
                    if result {
                        print("Log in successful")
                        self.performSegue(withIdentifier: self.segueIdentifier, sender: nil)
                    }
                    else{
                        //An alert with the error message will pop up
                        let controller = UIAlertController(
                            title: "Log in failed",
                            message: "User could not be found",
                            preferredStyle: .alert)
                        controller.addAction(UIAlertAction(
                                                title: "OK",
                                                style: .default,
                                                handler: nil))
                        self.present(controller, animated: true, completion: nil)
                        print(error!.localizedDescription)
                    }
                }
            } else if buttonState == "Sign up" {
                if confirmTextField.text! == passwordTextField.text! {
                    guard let email = emailTextField.text,
                          let password = passwordTextField.text,
                          email.count > 6,
                          password.count > 6
                    else {
                        print("Status: Sign up failed")
                        return false
                    }
                    Auth.auth().createUser(withEmail: email, password: password) { user, error in
                        if error == nil {
                            Auth.auth().signIn(withEmail: email,
                                               password: password)
                            print("Sign up successful")
                            self.performSegue(withIdentifier: self.segueIdentifier, sender: nil)
                        }
                    }
                } else {
                    //An alert with the error message will pop up
                    let controller = UIAlertController(
                        title: "Sign up failed",
                        message: "Passwords do not match",
                        preferredStyle: .alert)
                    controller.addAction(UIAlertAction(
                                            title: "OK",
                                            style: .default,
                                            handler: nil))
                    present(controller, animated: true, completion: nil)
                    return false
                }
            }
        }
        return false
    }
    
    
}
