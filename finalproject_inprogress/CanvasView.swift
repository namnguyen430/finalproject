//
//  CanvasView.swift
//  ColoringBook-TEST1
//
//  Created by Rosales, Melina M on 11/16/21.
//

import UIKit

// structure thats going to be called when touch is sensed. Using the points and color to draw line.

struct TouchPointAndColor{
    // lines will have a color, a width, and points.
    var color: UIColor?
    var width: CGFloat?
    var points: [CGPoint]?
    
    init(color: UIColor, points: [CGPoint]){
        self.color = color
        self.points = points
    }
}

// Class that uses struct to draw liens
class CanvasView: UIView {
    
    // lines uses above struct
    var lines = [TouchPointAndColor]()
    // default values so we can change them later.
    var strokeWidth: CGFloat = 15.0
    var strokerColor: UIColor = .black
    var capVar : CGLineCap!
    
    // main meat
    override func draw(_ rect: CGRect) {
        super.draw(rect) // draw/ rect is the view space that we can draw on
        
        guard let context = UIGraphicsGetCurrentContext() else{
            return
        }
        
        // for each time we are moving, it addes the points. But if we aren't moving, and instead making a new line, do the else statement.
        lines.forEach{ (line) in
            for (i,p) in (line.points?.enumerated())!{
                if i == 0 {
                    context.move(to:p)
                } else{
                    context.addLine(to: p)
                }
                // set stroke and width , defaults included
                context.setStrokeColor(line.color?.cgColor ?? UIColor.black.cgColor)
                context.setLineWidth(line.width ?? 15.0)
            }
            // draw stroke
            context.setLineCap(capVar)
            //context.setLineCap(.round)
            context.strokePath()
        }
    }
    
    // line starts when touch begans.
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        lines.append(TouchPointAndColor(color: UIColor(), points: [CGPoint]()))
    }
    
    // so a line is basically an array of points. !!!
    // this function appends the points into the line list.
    // gaurd seems to be like a safer version of if let? can transter things out of scope.
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        guard let touch = touches.first?.location(in: nil) else{
            return
        }
        guard var lastPoint = lines.popLast() else{return}
        // takes note of lines characteristics before appending it.
        lastPoint.points?.append(touch)
        lastPoint.color = strokerColor
        lastPoint.width = strokeWidth
        lines.append(lastPoint)
        setNeedsDisplay() // tells system to update the view.
    }
    // wipes all lines on canvas and then updates view
    func clearCanvasView(){
        lines.removeAll()
        setNeedsDisplay()
    }
    
    // removes last line in the array (last line draw)
    // NOTE: for the redo button, i am going to store this line and put it into a seperate array
    func undoFunc(){
        // needs to check if there is any lines to be removed otherwise it freaks out.
        if lines.count > 0 {
            lines.removeLast()
            setNeedsDisplay()
        }
    }
    
    // for the 'new' button. Clears the array of lines so that that canvas is now empty
    func newFunc(){
        if lines.count>0{
            lines.removeAll()
            setNeedsDisplay()
        }
    }
    
}

