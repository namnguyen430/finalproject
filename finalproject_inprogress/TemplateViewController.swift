//
//  TemplateViewController.swift
//  finalproject_inprogress
//
//  Created by Soo Jung Aguilar on 11/30/21.
//

import UIKit

let reuseIdentifier = "imageCell"

class TemplateViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource {
    
    @IBOutlet weak var imageCollectionView: UICollectionView!
    
    var imageData: [String] = ["template1", "template2", "template3", "template4"]
    let lineSegue = "SelectedTemplateSegue"
    var delegate: UIViewController!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        imageCollectionView.delegate = self
        imageCollectionView.dataSource = self
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return imageData.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = imageCollectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath) as! TemplateCollectionViewCell
        
        let image = imageData[indexPath.row]
        cell.imageViewCell.image = UIImage(named: image)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let otherVC = delegate as! templateImageChanger
        otherVC.changeToTemplate(templateImageName: imageData[indexPath.row])
        self.dismiss(animated: true, completion: nil)
    }
    

}
