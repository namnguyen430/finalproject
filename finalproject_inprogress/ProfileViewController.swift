//
//  ProfileViewController.swift
//  homePage
//
//  Created by Nam Nguyen on 11/24/21.
//

import UIKit

class ProfileViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate, UIScrollViewDelegate {
    @IBOutlet weak var circularImageView: UIImageView!
    @IBOutlet weak var imageButton: UIButton!
    @IBOutlet weak var imageView: UIView!
    
    @IBOutlet weak var likeCountLabel: UILabel!
    @IBOutlet weak var likeLabel: UILabel!
    
    @IBOutlet weak var lineArtCountLabel: UILabel!
    @IBOutlet weak var lineArtLabel: UILabel!
    
    @IBOutlet weak var colorPageCountLabel: UILabel!
    @IBOutlet weak var colorPageLabel: UILabel!
    
    @IBOutlet weak var userNameLabel: UILabel!
    
    @IBOutlet weak var myLineArtLabel: UILabel!
    @IBOutlet weak var myColoringPageLabel: UILabel!
    
    @IBOutlet weak var lineArtButton: UIButton!
    
    @IBOutlet weak var LAHoriztontalStack: UIStackView!
    
    @IBOutlet weak var LALeftVertStack: UIStackView!
    @IBOutlet weak var LAMiddleVertStack: UIStackView!
    @IBOutlet weak var LARightVertStack: UIStackView!
    
    @IBOutlet weak var CPHorizontalStack: UIStackView!
    
    @IBOutlet weak var CPLeftVertStack: UIStackView!
    @IBOutlet weak var CPMiddleVertStack: UIStackView!
    @IBOutlet weak var CPRightVertStack: UIStackView!
    
    
    let picker = UIImagePickerController()
    var scrollView: UIScrollView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        scrollView = UIScrollView(frame: view.bounds)
        scrollView.contentOffset = CGPoint(x: 1000, y: 450)
        
        picker.delegate = self
        scrollView.delegate = self
        
        imageView.layer.cornerRadius = imageView.frame.size.height / 2
        
        circularImageView.layer.cornerRadius = circularImageView.frame.size.height / 2
        circularImageView.clipsToBounds = true
        circularImageView.layer.masksToBounds = true
        circularImageView.layer.borderColor  = UIColor.lightGray.cgColor
        circularImageView.layer.borderWidth = 5.0
        
        imageButton.layer.cornerRadius = imageButton.frame.size.height / 2
        imageButton.layer.masksToBounds = true
        
        likeCountLabel.text = "300"
        likeLabel.text = "Likes"
        
        lineArtCountLabel.text = "30"
        lineArtLabel.text = "Line Art"
        
        colorPageCountLabel.text = "10"
        colorPageLabel.text = "Coloring Pages"
        
        userNameLabel.text = "@namasagi"
        
        myLineArtLabel.text = "My Line Art"
        
        myColoringPageLabel.text = "My Coloring Pages"
        
        // Grid Spacing for Line Art and Coloring Pages
        LAHoriztontalStack.spacing = 15.0
        LALeftVertStack.spacing = 15.0
        LAMiddleVertStack.spacing = 15.0
        LARightVertStack.spacing = 15.0
        
        CPHorizontalStack.spacing = 8.0
        CPLeftVertStack.spacing = 8.0
        CPMiddleVertStack.spacing = 8.0
        CPRightVertStack.spacing = 8.0
        
    }
    
    @IBAction func onImageButtonPressed(_ sender: Any) {
        
        picker.allowsEditing = false
        picker.sourceType = .photoLibrary
        present(picker, animated: true, completion: nil)
    
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        let chosenImage = info[.originalImage] as! UIImage
        
        circularImageView.contentMode = .scaleAspectFit
        circularImageView.image = chosenImage
        
        dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
    // adjust profile pic to fit circle
    // change like count depending on user
    // change line art count depending on user
    // change color page count depending on user
    // import line art by user
    // import coloring page by user
    // line art see more expands to another screen
    // coloring page see more expands to another screen
}
