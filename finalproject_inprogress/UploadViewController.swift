//
//  UploadViewController.swift
//  finalproject_inprogress
//
//  Created by Soo Jung Aguilar on 11/28/21.
//

import UIKit
import AVFoundation

protocol templateImageChanger {
    func changeToTemplate (templateImageName: String)
}

class UploadViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate, templateImageChanger {

    @IBOutlet weak var lineArtImageView: UIImageView!
    let picker = UIImagePickerController()
    let templateSegueIdentifier = "TemplateSegueIdentifier"
    let colorSegueIdentifier = "colorSegue"
    
    var viewImage: UIImage?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        picker.delegate = self
    }
    
    @IBAction func uploadButton(_ sender: Any) {
        picker.allowsEditing = false
        picker.sourceType = .photoLibrary
        present(picker, animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        let chosenImage = info[.originalImage] as! UIImage
        lineArtImageView.contentMode = .scaleAspectFit
        viewImage = chosenImage
        changeImage()
        dismiss(animated: true, completion: nil)
        
    }
    
    func changeImage() {
        lineArtImageView.image = viewImage
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
    func changeToTemplate(templateImageName: String) {
        viewImage = UIImage(named: templateImageName)
        changeImage()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == templateSegueIdentifier,
           let nextVC = segue.destination as? TemplateViewController {
            nextVC.delegate = self
        } else if segue.identifier == colorSegueIdentifier,
           let nextVC = segue.destination as? ColorViewController{
            nextVC.delegate = self
            if viewImage != nil {
                nextVC.bgImage = viewImage
                nextVC.isColor = true
            }
        }else if segue.identifier == "lineArtID",
                 let nextVC = segue.destination as? ColorViewController{
            nextVC.isColor = false        }
    }
    
    @IBAction func templateButton(_ sender: Any) {
    }
    
    @IBAction func unwindToMain(segue: UIStoryboardSegue){
        
    }
    
    @IBAction func coloringButtonPressed(_ sender: Any) {
        //Check if lineArtImageView is empty before performing segue
    }
    
}
