//
//  ExploreCollectionViewCell.swift
//  finalproject_inprogress
//
//  Created by Soo Jung Aguilar on 12/3/21.
//

import UIKit

class ExploreCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var imageView: UIImageView!
}
