//
//  TemplateCollectionViewCell.swift
//  finalproject_inprogress
//
//  Created by Soo Jung Aguilar on 11/30/21.
//

import UIKit

class TemplateCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var imageViewCell: UIImageView!
}
